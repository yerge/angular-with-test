import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.scss']
})
export class UserDeleteComponent implements OnInit, OnDestroy {

  paramSub: Subscription;
  
  constructor(private activateRoute: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit(): void {
    this.paramSub = this.activateRoute.params
                        .subscribe((userToRemove) => {
                          this.userService.deleteUser(userToRemove.id)
                          .subscribe();
                        });
  }

  ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }
}
