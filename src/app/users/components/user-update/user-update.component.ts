import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.interface';
import { UserService } from 'src/app/services/user.service';
import { UserUtils } from 'src/app/utils/userUtils';
import {Location} from '@angular/common';
import { rangeValidator } from 'src/app/validators/range-validator.directive';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {

  updateSub: Subscription;

  public enableSubmit: boolean = true;

  updatedUser: User;

  userToUpdate: User;

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private activateRoute: ActivatedRoute,
              private router: Router,
              private _location: Location,
              private userService: UserService) { }


  ngOnInit(): void {

    this.updateSub = this.activateRoute.params
      .subscribe(data => {
          this.userService.getUserById(data.id)
          .subscribe((data: User) => {
          this.userToUpdate = data;
          this.initForm();

          this.userForm.valueChanges
            .subscribe((value: User) => {
              console.log("value change");
              console.log(value)
              let lUser  = new User(value);
              //const isEqual = lUser.deepEqualUser(this.userToUpdate);
              const isEqual = UserUtils.deepEqualUser(lUser, this.userToUpdate);
              console.log(isEqual);

              if (isEqual) {
                this.enableSubmit = true;
              } else {
                this.enableSubmit = false;
              }
          });

          this.userForm.controls.id.disable();

          console.log("ngOnInit :");
          console.log(this.userToUpdate);
        });
      });
  }

  private backClicked() {
    this._location.back();
  }

  private initForm(): void {
    this.userForm = this.formBuilder.group({
      id: [this.userToUpdate.id],
      name: [this.userToUpdate.name, [Validators.required, Validators.minLength(3), Validators.maxLength(64)]],
      surname: [this.userToUpdate.surname, [Validators.required, Validators.minLength(3), Validators.maxLength(64)]],
      age: [this.userToUpdate.age, [Validators.required, rangeValidator(18, 150)]],
      profession: [this.userToUpdate.profession, Validators.required],
      address: this.formBuilder.group({
        roadNumber: [this.userToUpdate.address.roadNumber, [Validators.required, rangeValidator(1, 999)]],
        street: [this.userToUpdate.address.street, Validators.required],
        postalCode: [this.userToUpdate.address.postalCode, [Validators.required, rangeValidator(1000, 99999)]],
        city: [this.userToUpdate.address.city, Validators.required],
        country: [this.userToUpdate.address.country, Validators.required]
      }),
      mail: [this.userToUpdate.mail, Validators.email],
      phone: [this.userToUpdate.phone]
    });
  }

  onSubmit() {
    this.updatedUser = new User(this.userForm.value);

    // update the id because its control is disabled ?
    // option 2
    // this.updatedUser.id = this.userForm.controls.id.value;
  
    this.updatedUser.id = this.userForm.getRawValue().id;
    this.userService.updateUser(this.updatedUser.id, this.updatedUser)
      .subscribe();
    
      this.router.navigate(['/users']);
    //this.backClicked();
    //this.update.emit(this.updatedUser);
  }

  get name() {
    return this.userForm.get('name');
  }  

  get age() {
    return this.userForm.get('age');
  }
  
  get surname() {
    return this.userForm.get('surname');
  }

  get profession() {
    return this.userForm.get('profession');
  }

  get mail() {
    return this.userForm.get('mail');
  }

  get address() {
    return this.userForm.controls.address as FormGroup;
  }

}
