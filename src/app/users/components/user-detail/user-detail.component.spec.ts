import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { RouterTestingModule } from "@angular/router/testing";

import { UserDetailComponent } from './user-detail.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { User } from 'src/app/models/user.interface';

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ 
        RouterModule.forRoot([]),
        HttpClientTestingModule
      ],
      declarations: [ UserDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
    const user :User =
    {
      id: 0,
      "name": "Shah Rukh",
      "surname": "Khan",
      "age": 55,
      "profession": "Actor",
      "address": {
          "roadNumber": 8,
          "street": "Route de la Narbonne",
          "postalCode": 1000,
          "city": "Geneva",
          "country": "Switzerland"
        },
        "mail": "shah@test.in",
        "phone": "079 123 45 67"
    };

    component.detail = user;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have <h2> with "SHAH RUKH Details"', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const h2 = bannerElement.querySelector('h2');
    expect(h2.textContent).toEqual('SHAH RUKH Details');
  });
});
