import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {

  detail: User;

  paramSub: Subscription;
  
  constructor(private activateRoute: ActivatedRoute,
    private userService: UserService) { 

  }

  ngOnInit(): void {
    // get the user id detail by id
    this.paramSub = this.activateRoute.params.subscribe(data => {
      this.userService.getUserById(data.id)
      .subscribe((data: User) => this.detail = data);
    });
  }

  public ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }

}
