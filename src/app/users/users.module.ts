import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserService } from '../services/user.service';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserDeleteComponent } from './components/user-delete/user-delete.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserUpdateComponent } from './components/user-update/user-update.component';


@NgModule({
  declarations: [UsersComponent, UserDetailComponent, UserDeleteComponent, UserCreateComponent, UserUpdateComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ UserService ]
})
export class UsersModule { }
