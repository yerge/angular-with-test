import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { UserDeleteComponent } from './components/user-delete/user-delete.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserUpdateComponent } from './components/user-update/user-update.component';
import { UsersComponent } from './users.component';


const routes: Routes = [
  { path: '', 
    component: UsersComponent ,
    children: [
      {
        path: 'detail/:id',
        component: UserDetailComponent
      },
      {
        path: 'remove/:id',
        component: UserDeleteComponent
      },
      {
        path: 'edit/:id',
        component: UserUpdateComponent
      },
      {
        path: 'create',
        component: UserCreateComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
