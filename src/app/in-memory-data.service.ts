import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const users = [

      {
        "id": 0,
        "name": "Philipps",
        "surname": "Kevin",
        "age": 24,
        "profession": "Military",
        "address": {
            "roadNumber": 8,
            "street": "Route de la Narbonne",
            "postalCode": 1000,
            "city": "Geneva",
            "country": "Switzerland"
          },
          "mail": "kevin@test.uk",
          "phone": "079 123 45 67"
      },
      {
        "id": 1,
        "name": "Carla",
        "surname": "Carlita",
        "age": 35,
        "profession": "Politian",
        "address": {
            "roadNumber": 10,
            "street": "Route de Versoix",
            "postalCode": 2000,
            "city": "Lausanne",
            "country": "Switzerland"
          },
          "mail": "carlita@test.ch",
          "phone": "079 123 45 68"
      },
      {
        "id": 2,
        "name": "Raul",
        "surname": "Garcia",
        "age": 25,
        "profession": "Player",
        "address": {
            "roadNumber": 11,
            "street": "Route de la catalogne",
            "postalCode": 3000,
            "city": "Barcelona",
            "country": "Spain"
          },
          "mail": "raul@test.cat",
          "phone": "079 123 45 69"
      },
      {
        "id": 3,
        "name": "Raj",
        "surname": "Malhotra",
        "age": 55,
        "profession": "Actor",
        "address": {
            "roadNumber": 12,
            "street": "Route de l'Inde",
            "postalCode": 4000,
            "city": "Amristar",
            "country": "India"
          },
          "mail": "raj@test.in",
          "phone": "079 123 45 70"
      },
      {
        "id": 4,
        "name": "Frank",
        "surname": "LeBoeuf",
        "age": 45,
        "profession": "Player",
        "address": {
            "roadNumber": 13,
            "street": "Route de la France",
            "postalCode": 5000,
            "city": "Paris",
            "country": "France"
          },
          "mail": "frank@test.fr",
          "phone": "079 123 45 71"
      },
      {
        "id": 5,
        "name": "Messi",
        "surname": "Leo",
        "age": 32,
        "profession": "Player",
        "address": {
            "roadNumber": 14,
            "street": "Route de la Catalogne",
            "postalCode": 6000,
            "city": "Barcelona",
            "country": "Spain"
          },
          "mail": "leo@test.cat",
          "phone": "079 123 45 72"
      },
      {
        "id": 6,
        "name": "Cristiano",
        "surname": "Ronaldo",
        "age": 34,
        "profession": "Player",
        "address": {
            "roadNumber": 15,
            "street": "Route de la vieille dame",
            "postalCode": 7000,
            "city": "Turin",
            "country": "Italia"
          },
          "mail": "ronaldo@test.it",
          "phone": "079 123 45 73"
      },
      {
        "id": 7,
        "name": "Prem",
        "surname": "Khurrana",
        "age": 54,
        "profession": "Actor",
        "address": {
            "roadNumber": 16,
            "street": "Route du Mumbai",
            "postalCode": 8000,
            "city": "Mumbai",
            "country": "India"
          },
          "mail": "prem@test.in",
          "phone": "079 123 45 74"
      },
      {
        "id": 8,
        "name": "Chopra",
        "surname": "Yash",
        "age": 84,
        "profession": "Director/Producer",
        "address": {
            "roadNumber": 17,
            "street": "Route de Amristar",
            "postalCode": 9000,
            "city": "Amristar",
            "country": "India"
          },
          "mail": "yash@test.in",
          "phone": "079 123 45 75"
      },
      {
        "id": 9,
        "name": "Trump",
        "surname": "Donald",
        "age": 74,
        "profession": "Politian/Businessman",
        "address": {
            "roadNumber": 18,
            "street": "Route de New York",
            "postalCode": 10000,
            "city": "Washington",
            "country": "USA"
          },
          "mail": "donald@test.com",
          "phone": "079 123 45 76"
      },
      {
        "id": 10,
        "name": "Macron",
        "surname": "Emmanuel",
        "age": 42,
        "profession": "Politian",
        "address": {
            "roadNumber": 19,
            "street": "Route de l'Elysée",
            "postalCode": 10000,
            "city": "Paris",
            "country": "France"
          },
          "mail": "emmanuel@test.fr",
          "phone": "079 123 45 77"
      }

    ];
    return {users};
  }
}
