import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from './user.service';
import { User } from '../models/user.interface';

const USER_API = "api/users";

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an Observable<User[]>', () => {
    const data :User[] = [

      {
        id: 0,
        "name": "Philipps",
        "surname": "Kevin",
        "age": 24,
        "profession": "Military",
        "address": {
            "roadNumber": 8,
            "street": "Route de la Narbonne",
            "postalCode": 1000,
            "city": "Geneva",
            "country": "Switzerland"
          },
          "mail": "kevin@test.uk",
          "phone": "079 123 45 67"
      },
      {
        "id": 1,
        "name": "Carla",
        "surname": "Carlita",
        "age": 35,
        "profession": "Politian",
        "address": {
            "roadNumber": 10,
            "street": "Route de Versoix",
            "postalCode": 2000,
            "city": "Lausanne",
            "country": "Switzerland"
          },
          "mail": "carlita@test.ch",
          "phone": "079 123 45 68"
      }
    ];

    service.getUsers().subscribe(users => {
      expect(users.length).toBe(2);
    });

    const req = httpMock.expectOne(`${USER_API}`);
    expect(req.request.method).toBe("GET");
    req.flush(data);

  });

  it('should return an Observable<User>', () => {
    const data :User =
    {
      id: 0,
      "name": "Philipps",
      "surname": "Kevin",
      "age": 24,
      "profession": "Military",
      "address": {
          "roadNumber": 8,
          "street": "Route de la Narbonne",
          "postalCode": 1000,
          "city": "Geneva",
          "country": "Switzerland"
        },
        "mail": "kevin@test.uk",
        "phone": "079 123 45 67"
    };

    service.getUserById(0).subscribe(user => {
      expect(user.id).toBe(0);
      expect(user.name).toBe('Philipps');
      expect(user.mail).toBe('kevin@test.uk');
    });

    const req = httpMock.expectOne(`${USER_API}/0`);
    expect(req.request.method).toBe("GET");
    req.flush(data);

  });

  
  it('should removea user with id=0', () => {
    const data :User[] = [

      {
        id: 0,
        "name": "Philipps",
        "surname": "Kevin",
        "age": 24,
        "profession": "Military",
        "address": {
            "roadNumber": 8,
            "street": "Route de la Narbonne",
            "postalCode": 1000,
            "city": "Geneva",
            "country": "Switzerland"
          },
          "mail": "kevin@test.uk",
          "phone": "079 123 45 67"
      },
      {
        "id": 1,
        "name": "Carla",
        "surname": "Carlita",
        "age": 35,
        "profession": "Politian",
        "address": {
            "roadNumber": 10,
            "street": "Route de Versoix",
            "postalCode": 2000,
            "city": "Lausanne",
            "country": "Switzerland"
          },
          "mail": "carlita@test.ch",
          "phone": "079 123 45 68"
      }
    ];

    service.deleteUser(1).subscribe(user => {
      expect(data.length).toBe(1);
      expect(data[0].id).toBe(0);
    });

    //const req = httpMock.expectOne(`${USER_API}/`);
    const req = httpMock.match(`${USER_API}/1`);
    console.log(req[1].request.method);
    expect(req[1].request.method).toBe("DELETE")
    /* expect(req[0].request.method).toBe("DELETE");
    console.log(req[1].request.method);
    expect(req[1].request.method).toBe("DELETE"); */
    //req[1].flush(data);

  });
  
  
  it('should update user with id=0', () => {
    const data :User =
    {
      id: 0,
      "name": "Philipps",
      "surname": "Kevin",
      "age": 24,
      "profession": "Military",
      "address": {
          "roadNumber": 8,
          "street": "Route de la Narbonne",
          "postalCode": 1000,
          "city": "Geneva",
          "country": "Switzerland"
        },
        "mail": "kevin@test.uk",
        "phone": "079 123 45 67"
    };

    const userToUpdated: User = {
      id: 0,
      "name": "Shah Rukh",
      "surname": "Khan",
      "age": 54,
      "profession": "Military",
      "address": {
          "roadNumber": 10,
          "street": "Route de la Narbonne",
          "postalCode": 1000,
          "city": "Geneva",
          "country": "Switzerland"
        },
        "mail": "shah@test.in",
        "phone": "079 123 45 67"
    };

    service.updateUser(0, userToUpdated).subscribe(user => {
      expect(user.id).toBe(0);
      /** question about update */
    });

    const req = httpMock.expectOne(`${USER_API}/0`);
    expect(req.request.method).toBe("PUT");
    req.flush(data);

  });

  it('should create a new user', () => {
    const data :User =
    {
      id: 0,
      "name": "Shah Rukh",
      "surname": "Khan",
      "age": 55,
      "profession": "Actor",
      "address": {
          "roadNumber": 8,
          "street": "Route de la Narbonne",
          "postalCode": 1000,
          "city": "Geneva",
          "country": "Switzerland"
        },
        "mail": "shah@test.in",
        "phone": "079 123 45 67"
    };


    service.createUser(data).subscribe(user => {
      console.log("create user :");
      console.log(user);
      expect(user.id).toBe(0);
      expect(user.name).toBe("Shah Rukh");
      expect(user.surname).toBe("Khan");
      expect(user.profession).toBe("Actor");
      expect(user.mail).toBe("shah@test.in");
      /** question about update */
    });

    const req = httpMock.expectOne(`${USER_API}`);
    expect(req.request.method).toBe("POST");
    req.flush(data);

  });


  afterEach(() => {
    httpMock.verify();
  });
});
