import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObjectMapper } from 'json-object-mapper';
import { Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { User } from '../models/user.interface';


const USER_API = "api/users";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _onUserUpdated: Subject<User> = new Subject();
  private _onUserDeleted: Subject<User> = new Subject();
  private _onUserCreated: Subject<User> = new Subject();

  constructor(private http: HttpClient) { }

  public onUserUpdated$(): Observable<User> {
    return this._onUserUpdated.asObservable();
  }

  public onUserDeleted$(): Observable<User> {
    return this._onUserDeleted.asObservable();
  }

  public onUserCreated$(): Observable<User> {
    return this._onUserCreated.asObservable();
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(USER_API)
      .pipe(
        map((data: User[]) => {
          let result = ObjectMapper.deserializeArray<User>(User, data);
          return result;
        })
        //catchError(this.handleError)
      );
  }

  getUserById(id: Number): Observable<User> {

    return this.http.get<User>(`${USER_API}/${id}`)
      .pipe(
        map((data: User) => ObjectMapper.deserialize<User>(User, data))
        //catchError(this.handleError)
      );
  }

  deleteUser(id: Number): Observable<User> {
    let userToDelete: User;
    this.getUserById(id)
          .subscribe((data: User) => {
            userToDelete = data;
    });

    return this.http
      .delete<User>(`${USER_API}/${id}`)
      .pipe(
        tap(() => this._onUserDeleted.next(userToDelete))
        //catchError(this.handleError)
      );
  }

  updateUser(id: number, user: User): Observable<User> {
    const currentUser: User = user;
    return this.http.put<User>(`${USER_API}/${id}`, user)
      .pipe(
        //map((data) => ObjectMapper.serialize(data)),
        tap(() => this._onUserUpdated.next(currentUser))
        //catchError(this.handleError)
      );
  }

  createUser(user: User): Observable<User> {
    const currentUser: User = user;
    return this.http.post<User>(USER_API, user)
      .pipe(
        tap(() => this._onUserCreated.next(currentUser))
        //catchError(this.handleError)
      );
  }
  
}
